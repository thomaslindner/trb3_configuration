#!/bin/bash
## TRIUMF TRB3 startup script
## Thome Lindner, Sept 2017
## Script is annotated to explain what is being done.
##
## To execute this script do
## source startup.sh


DAQ_TOOLS_PATH=~/trbsoft/daqtools
USER_DIR=~/trbsoft/daqtools/users/triumf_trb171
TRB_WEB_DIR=$DAQ_TOOLS_PATH/web

export PATH=$PATH:$DAQ_TOOLS_PATH
export PATH=$PATH:$DAQ_TOOLS_PATH/tools
export PATH=$PATH:$USER_DIR
export PATH=$PATH:/home/tbr3_user/trbsoft/trb3/dabc/bin/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/tbr3_user/trbsoft/trb3/dabc/lib/

### TL
### This part restarts the trbnetd server;
### this server takes instructions from trbcmd executable and
### passes them actually to the TRB.
export TRB3_SERVER=trb171:26000
export TRBNETDPID=$(pgrep -f "trbnetd -i 171")

export DAQOPSERVER=localhost:171

echo "- trbnetd pid: $TRBNETDPID"

if [[ -z "$TRBNETDPID" ]] 
then
    ~/trbsoft/trbnettools/bin/trbnetd -i 171
else
    echo "trbnetd already running"
fi

#./check_ping.pl --reboot

### TL
### Reset the FPGAs on the TRB3 itself
echo "reset"
./trbreset_loop.pl

# sleep 1;


##################################################
## Set addresses
## TL
## this set somehow "maps trbnet-addresses to serial number + FPGA numbers"
##################################################
merge_serial_address.pl $DAQ_TOOLS_PATH/base/serials_trb3.db $USER_DIR/db/addresses_trb3.db



## TL
## Seems clear that these calls seem to set details about the TRB3
## gigabit communication.
## configgbe.db is supposed to define the network connection for the TRB3.
## in particular, since we use UDP packets, we need to hardcode the MAC
## address of the event builder PC; daq14 in our case.
echo "GbE settings"
loadregisterdb.pl db/register_configgbe.db
# loadregisterdb.pl db/register_configgbe_ip.db #daq14
loadregisterdb.pl db/register_configgbe_ip_daq11.db  # daq11

## TL
## With this setting we define which type of extension boards we
## have installed; GPIN and padiwa in our case.
## Details here are a little fuzzy.  Sort of described in
## section "Slow Control registers"
echo "TDC settings"
loadregisterdb.pl db/register_configtdc.db
echo "TDC settings end"

# set correct timeout: off for channel 0, 1, 2sec for 2
# TL: I don't know what this does...
trbcmd w 0xfffe 0xc5 0x50ff


echo "pulser"
# TL Set the pulser frequency
# I can't find the right documentation on this register...
# pulser #0 to 10 kHz
trbcmd w 0xc001 0xa156 0x0000270f   
#trbcmd w 0xc001 0xa156 0x0000070f   
#trbcmd w 0xc001 0xa150 0x0022270f   


# TL No full description of this XML-DB... not sure what this does...
# Gives error message, so comment out
#cd ~/trbsoft/daqtools/xml-db
#./put.pl Readout 0xfe51 SetMaxEventSize 500
#cd $USER_DIR

# TL also can't find documentation on this register
# Gives error message, so comment out...
#trbcmd w 0xfe51 0xdf80 0xffffffff # enable monitor counters


echo "pulser enable"
# pulser enable
trbcmd setbit 0xc001 0xa101 0x2


