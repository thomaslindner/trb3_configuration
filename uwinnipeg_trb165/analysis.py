#!/usr/bin/python


remaining_data = []
ch1_time = 0.0
ch3_time = 0.0

print " {\n {\n TH1F *h1 = new TH1F(\"h1\",\"h1\",100,0,5); "

for line in open("output_test_ch1_3.txt",'r'):

    elements = line.split(" ")
    
    if line.find("ch: 1") > -1 or line.find("ch: 3") > -1:
        timestr = elements[20]
        # print timestr
        time = 0.0
        try:
            time = float(timestr[3:])
        except ValueError:
            time = 0.0
        #print time,elements[20]
        if elements[16] == '1':
            ch1_time = time
        if elements[16] == '3':
            ch3_time = time
            
            #print ch1_time, ch3_time, ch1_time-ch3_time
            print "h1->Fill(" + str(ch1_time-ch3_time) + ");"
                                
        

        
print "h1->Draw();"
